#!/bin/bash

git init # watchout if rerun
echo "node-modules" >> .gitignore
echo "logs" >> .gitignore
echo ".DS_Store" >> .gitignore
npm init -y

npm install --save winston@3.12.0 express@4.18.2
npm install --save-dev mocha@10.3.0 chai@5.1.0

npm pkg set 'type'="module"
npm pkg set 'scripts.start'="node src/main.mjs"
# \ tarkoittaa että jatkuu seuraavalla rivillä
npm pkg set 'scripts.dev'=\
"node --watch-path=src --watch src/main.mjs"
npm pkg set 'scripts.test'="mocha --timeout 5000"

mkdir src test
touch rest.http

touch src/counter.mjs
touch src/main.mjs
touch src/logger.mjs
touch src/router.mjs
touch src/endpoint.mw.mjs

touch test/counter.spec.mjs

# ./setup.sh
# npm run dev

# npm run test

# git remote add origin https://gitlab.com/HeidiLaaksonen/L10_TALLY_COUNTER
# git remote -v 

# jos pitää muuttaa 
# git remote set-url origin https://gitlab.com/HeidiLaaksonen/L10_TALLY_COUNTER

# git status
# git add .
# git commit -m "initial commit"
# git push
# git push --set-upstream origin master
