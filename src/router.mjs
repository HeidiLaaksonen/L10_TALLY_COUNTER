import express from 'express';
import Counter from './counter.mjs';
import logger from './logger.mjs';
import enpoint_mw from './endpoint.mw.mjs';

const app = express();
const counter = new Counter();

//Middleware
app.use(enpoint_mw);

//Endpoints
app.get('', (reg, res) => res.send("Welcome!"));
// GET /counter-increase
app.get('/counter-increase', (reg, res) => {
    counter.increase();
    // send lähettää ainoastaan string joten pistetään `` sisään jotta se osaa lähettää sen
    res.send(`${counter.read()}`); 
});
// GET /counter-read
app.get('/counter-read', (reg, res) => {
    logger.http("[ENDPOINT] GET /counter-read");
    // logger.log("http", "[ENDPOINT] GET /counter-read");
    const url = req.url; // <- mikä toi on? !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    const method = req.method; // <- voiko hyödyntää? 
    res.send(`${counter.read()}`); 
});
// GET /counter-zero
app.get('/counter-zero', (reg, res) => {
    counter.zero();
    res.send(`${counter.read()}`); 
});

app.all('*', (req, res) => {
    res.status(404).send("Resourse not found.");
});

export default app;
