// import { describe } from "mocha"; // OMALLA KONEELLA mocha pitää olla kommentoitu jotta toimii
import { expect } from "chai";

import Counter from "../src/counter.mjs";

const counter = new Counter();

describe('Counter tests', () => {
    it('Initial count is zero', () => {
        const expected_zero = 0;
        expect(counter.read())
            .to
            .equal(expected_zero);
    });
    it('Can add to the counter', () => {
        counter.increase();
        const expected_count = 1;
        expect(counter.read())
            .to
            .equal(expected_count);
    });
    it('Can add to the counter and zero it', () => {
        counter.increase();
        counter.zero();
        const expect_zero = 0;
        expect(counter.read())
            .to
            .equal(expect_zero);
    });
}); 
